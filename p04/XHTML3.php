<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML, 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="htt://www.w3.org/1999/xhtml" xml:lang="es">
<head>
    <meta http-equiv="content-Type" content="text/html" charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Practica 4 - Funciones</title>
</head>
<body>
    
    <?php


        $informacion1 = $_POST['informacion'];
        $mostrar = $_POST['mostrar'];
        //ejercicio 6
   
        $autos = array(
            'AAA1001'=> array(
                'Auto' => array(
                    'Marca' => 'HONDA',
                    'Modelo' => 2021,
                    'Tipo' => 'Camioneta'),
                'Propietario' =>  array(
                    'Nombre' => 'Juan Carlos Conde Ramirez',
                    'Ciudad' => 'Puebla',
                    'Direccion' => 'Av 115')
            ),

            'AAA1002'=> array(
                'Auto' => array(
                    'Marca' => 'AUDI',
                    'Modelo' => 2022,
                    'Tipo' => 'Camioneta'),
                'Propietario' =>  array(
                    'Nombre' => 'Gerardo Jiménez Domingues',
                    'Ciudad' => 'Puebla',
                    'Direccion' => 'Av 119 oriente')
            ),

            'AAA1003'=> array(
                'Auto' => array(
                    'Marca' => 'NISSAN',
                    'Modelo' => 2021,
                    'Tipo' => 'Sedan'),
                'Propietario' =>  array(
                    'Nombre' => 'Herlinda Bazán Ramos',
                    'Ciudad' => 'México',
                    'Direccion' => 'Mayorazgo, calle 3 sur')
            ),

            'AAA1004'=> array(
                'Auto' => array(
                    'Marca' => 'MAZDA',
                    'Modelo' => 2019,
                    'Tipo' => 'Sedan'),
                'Propietario' =>  array(
                    'Nombre' => 'Fernanda Jiménez Bázan',
                    'Ciudad' => 'Puebla',
                    'Direccion' => 'C.U, av San Claudio')
            ),

            'AAA1005'=> array(
                'Auto' => array(
                    'Marca' => 'HIUNDAY',
                    'Modelo' => 2020,
                    'Tipo' => 'camioneta'),
                'Propietario' =>  array(
                    'Nombre' => 'Ximena Jiménez Bázan',
                    'Ciudad' => 'Puebla',
                    'Direccion' => 'C.U, av San Claudio')
            ),

            'AAA1006'=> array(
                'Auto' => array(
                    'Marca' => 'AUDI',
                    'Modelo' => 2020,
                    'Tipo' => 'Hachback'),
                'Propietario' =>  array(
                    'Nombre' => 'Karla Romero',
                    'Ciudad' => 'Puebla',
                    'Direccion' => 'Fraccionamiento Heroes')
            ),

            'AAA1007'=> array(
                'Auto' => array(
                    'Marca' => 'AUDI',
                    'Modelo' => 2022,
                    'Tipo' => 'camioneta'),
                'Propietario' =>  array(
                    'Nombre' => 'Britany Itaii Perez Cadena',
                    'Ciudad' => 'Puebla',
                    'Direccion' => 'Calle 2 oriente, Col centro Historico')
            ),

            'AAA1008'=> array(
                'Auto' => array(
                    'Marca' => 'NISSAN',
                    'Modelo' => 2022,
                    'Tipo' => 'camioneta'),
                'Propietario' =>  array(
                    'Nombre' => 'Fatima Jiménez Bázan',
                    'Ciudad' => 'Puebla',
                    'Direccion' => 'C.U, av San Claudio')
            ),

            'AAA1009'=> array(
                'Auto' => array(
                    'Marca' => 'MAZDA',
                    'Modelo' => 2019,
                    'Tipo' => 'coche'),
                'Propietario' =>  array(
                    'Nombre' => 'Alvaro Jiménez Flores',
                    'Ciudad' => 'Puebla',
                    'Direccion' => 'Mayorazgo calle 3, casa 2')
            ),

            'AAA1010'=> array(
                'Auto' => array(
                    'Marca' => 'NISSAN',
                    'Modelo' => 2010,
                    'Tipo' => 'coche'),
                'Propietario' =>  array(
                    'Nombre' => 'Oliva Dominguez Vazquez',
                    'Ciudad' => 'Puebla',
                    'Direccion' => 'Mayorazgo calle 3, casa 2')
            ),

            'AAA1011'=> array(
                'Auto' => array(
                    'Marca' => 'AUDI',
                    'Modelo' => 2010,
                    'Tipo' => 'coche'),
                'Propietario' =>  array(
                    'Nombre' => 'Agustin Bazan Bautista',
                    'Ciudad' => 'Puebla',
                    'Direccion' => 'Amalucan, retorno fresno')
            ),

            'AAA1012'=> array(
                'Auto' => array(
                    'Marca' => 'AUDI',
                    'Modelo' => 2017,
                    'Tipo' => 'coche'),
                'Propietario' =>  array(
                    'Nombre' => 'Guadalupe Ramos Torres',
                    'Ciudad' => 'Puebla',
                    'Direccion' => 'Amalucan, retorno fresno')
            ),

            'AAA1013'=> array(
                'Auto' => array(
                    'Marca' => 'NAZDA',
                    'Modelo' => 2017,
                    'Tipo' => 'Hachback'),
                'Propietario' =>  array(
                    'Nombre' => 'Maria del Carmen Perez Jimenez',
                    'Ciudad' => 'Puebla',
                    'Direccion' => 'Fraccionamiento la Galaxia')
            ),

            'AAA1014'=> array(
                'Auto' => array(
                    'Marca' => 'NAZDA',
                    'Modelo' => 2017,
                    'Tipo' => 'Hachback'),
                'Propietario' =>  array(
                    'Nombre' => 'Maria del Carmen Perez Jimenez',
                    'Ciudad' => 'Puebla',
                    'Direccion' => 'Fraccionamiento la Galaxia')
            ),

            'AAA1015'=> array(
                'Auto' => array(
                    'Marca' => 'HIUNDAY',
                    'Modelo' => 20120,
                    'Tipo' => 'Hachback'),
                'Propietario' =>  array(
                    'Nombre' => 'Adolfo Rico Aguilar',
                    'Ciudad' => 'Puebla',
                    'Direccion' => 'calle 2 norte, col Centro historico')
            )

        );


        
        function autos($informacion1){ 
            global $autos;
            foreach($autos as $val => $datos){
                if($informacion1 == $val){
                    echo "La informacion de la matricula es la siguiente";
                    echo '<br>';
                    echo '<pre>';
                    print_r($val);
                    print_r($datos);
                    echo '</pre>';
                    echo '<br>';
                }
               //echo "Sin ingreso de matricula o no existe";
            }


        }

        autos($informacion1);
        
        function autos2($mostrar){
            $aux = 'mostrar';
            if ($aux === $mostrar) {
                echo " Decidiste mostrar los autos de la ciudad";
                global $autos;
                echo '<br>';
                echo '<pre>';
                print_r($autos);
                echo '</pre>';
                echo '<br>';
            }
            //echo "Sin sleccionar";
            
        }

        autos2($mostrar);
        
    ?>
</body>
</html>