<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML, 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="htt://www.w3.org/1999/xhtml" xml:lang="es">
<head>
    <meta http-equiv="content-Type" content="text/html" charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Practica 4 - Funciones</title>
</head>
<body>
    <form action="http://localhost/TW/XHTML.php" method="get">
        Introduce un número (ejercicio 1): <input type="text" name="numero"/> 
        <br>
        Introduce un número (ejercicio 3): <input type="text" name="numero3"/>
        <input type="submit" name="enviar">
        </br>
    </form>
    <?php
        //ejercicio 1
        
        $numero = $_GET['numero'];  //detro de get se coloca el nombre de la variable que se asigno dentro del XHTML
        function numero( $numero){
            if($numero%5==0 and $numero%7==0 ){
                echo $numero." es múltiplo de 5 y 7";
            }
            elseif($numero%5==0){
                echo $numero." es múltiplo de 5";
            }
            elseif ($numero%7==0) {
                echo $numero." es múltiplo de 7";
            }
            else{
                echo $numero." No es multiplo de 5 ni de 7";
            }
        } 
        
        numero($numero);

         //Ejercicio 2
         function aleatorio(){
            $i = 0;
            $array = array();
            do{
                $array[$i][0] = rand(1,100); 
                $array[$i][1] = rand(1,100);
                $array[$i][2] = rand(1,100);
                $i++;
            }while(($array[$i-1][0]%2 != 0) && ($array[$i-1][1]%2 == 0) && ($array[$i-1][2]%2 != 0));
            echo '<br>';
            echo '<pre>';
            print_r($array);
            echo '</pre>';
            echo '<br>';
            echo $i*3 . ' números obtenidos en '. $i . ' iteraciones.';
        }
        aleatorio();

        //ejercicio 3
        $numero3 = $_GET['numero3'];
        function aleatorio2($numero3){
            $num_aleatorio = rand(1,100);
            while ($numero3%$num_aleatorio != 0) {
                $num_aleatorio = rand(1,100);
            }
            echo "<br> <br>";
            echo 'El numero '. $numero3 . ' que brindaste, es multiplo de' . $num_aleatorio . ' de ese numero aleatorio';
        }
        aleatorio2($numero3);

        //ejercicio3.1
        function aleatorio3($numero3){
            do{
                $num_al = rand(1,100);
            }while($numero3%$num_al != 0);
            echo '<br><br>';
            echo 'El número '. $numero3 . ' que brindaste, es multiplo de '. $numero3 . ' de ese numero aleatorio';;
        }
        aleatorio3($numero3);

        //ejercicio 4
        function letrass(){
            $n=97;          
            $letras = array();
            for($z=97; $z<=122; $z++){              
                $letras[$z] = chr($n);
                $n +=1;
            }
            echo "<br> <br>";
            echo "<table border align = 'lefth'>";    //la tabla la imprime XHTML
            foreach ($letras as $clave => $elemento) {          
                echo "<td>";  
                echo $elemento;
                echo "</td>";                       
            }
            echo "</table>";            
        }

        letrass();
        
    ?>



</body>
</html>