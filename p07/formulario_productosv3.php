<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style type="text/css">
      ol, ul { 
          list-style-type: none;
      }

      body {
        margin: 100px; 
        background-image: linear-gradient(140deg, #2c75ff 0, #686cfc 4.55%, #8b63f1 9.09%, #a658e5 13.64%, #bb4cd7 18.18%, #cd40c8 22.73%, #db32b7 27.27%, #e623a6 31.82%, #ee1394 36.36%, #f30382 40.91%, #f60171 45.45%, #f60f5f 50%, #f41f4e 54.55%, #f02e3c 59.09%, #ea3b2b 63.64%, #e24716 68.18%, #d85300 72.73%, #ce5d00 77.27%, #c26600 81.82%, #b46e00 86.36%, #a67500 90.91%, #977c00 95.45%, #878100 100%);
        font-family: Verdana, Helvetica, sans-serif;
        font-size: 80%;}
        h1 {color: black;
            border-bottom: 1px solid white;}
            </style>
            <script type="text/javascript">
      function validarDatos(){
        //ESTE ARCHIVO SE ENVIA A GIT Y SE TOMA CAp

        // Variable de tipo booleano que ayudara a enviar el formulario solo si los datos son correctos true, de lo contrario este devolvera false*/
        //var todo_correcto = true; 

        /*El primer campo a comprobar es el del nombre. Lo traemos por id y verificamos
        la condición, en este caso, le decimos que tiene que tener más de 0 dígitos y menor de 100
        para que sea un nombre válido. Si no tiene más de 0 dígitos, la variable todo_correcto
        devolverá false.*/
        todo_correcto = true;
        if(document.getElementById('form-name').value.length > 100  || document.getElementById('form-name').value == ""){
            alert('El nombre es necesario')    
            todo_correcto = false;
        }

        /*if (document.getElementById('form-marca') == null || document.getElementById('form-marca') == "") {
          alert("La marca es obligatoria");
          todo_correcto = false;
      } */

        var validacion = /^[a-zA-Z0-9]+$/; //letras, numeros 
        //var modelo = document.getElementById('form-modelo');
        if (!validacion.test(document.getElementById('form-modelo').value) || document.getElementById('form-modelo').value.length > 25 || document.getElementById('form-modelo').value.length == "" ) { //si la var model no contine eexpresiones como la variable validacion retorna falso
          alert('Verifica el modelo, este debe ser menor o igual a 25 carcateres y sin espacios ');
          todo_correcto = false;
        }

      if(document.getElementById('form-detalles').value.length > 250){
          alert("Lod detalles deben tener menos de 250 carcateres");
          todo_correcto = false;
      }

        /*Para comprobar las unidades, utilizaremos la función isNaN(), que nos dirá si el valor
        ingresado NO es un número (NaN son las siglas de Not a Number). si unidades no es un
        número, todo_correcto será false.*/

        if(document.getElementById('form-unidades').value < 0 ||document.getElementById('form-unidades').value == ""){
          alert("Las unidades son obligatorias o las unidades deben ser un numero");
          
          todo_correcto = false;
        }

        if(document.getElementById('form-precio').value == 0 ){
          alert("Los productos deben tener un precio mayor a 99.99 o no ingresaste unprecio"); 
          todo_correcto = false;
        }
      

      if(todo_correcto === false){
        alert("Algunos campos son obligatorios");
      }else{
        alert("Registro exitoso");
      }

      return todo_correcto; 
}


</script>
<title>Formulario</title>
</head>
<body>
    <h1>Actualizacion / Registro </h1>
    <p>A continuación, se muestran los datos del producto</p>

    <form id="formProductos" onsubmit="return validarDatos();"  method="post" enctype="multipart/form-data" action="http://localhost/TW/p05/servidor.php">
        <fieldset>
          <legend>Recuerda que el id no se visualiza porque no se puede cambiar </legend>
          <ul>
            <li><label for="form-name">Nombre del producto:</label> <input type="text" name="nombre" id="form-name" placeholder="Celular" value="<?= !empty($_POST['nombre'])?$_POST['nombre']:$_GET['nombre'] ?>"></li>

            <li><label for="form-marca">Marca del producto:</label> 
              <select name="marca" id="form-marca" value="<?= !empty($_POST['marca'])?$_POST['marca']:$_GET['marca'] ?>">
                <option value="">Selecciona una marca</option>
                <option value="Xiaomi">Xiaomi</option>
                <option value="Realme">Realme</option>
                <option value="Umidigi">Umidigi</option>
                <option value="Motorola">Motorola</option>
                <option value="Samsung">Samsung</option>
                <option value="Iphone">Iphone</option>
                <option value="VIVO">VIVO</option>
                <option value="TCL">TCL</option>
                <option value="TecnoSpark">TecnoSpark</option>
            </select></li>


            <li><label for="form-modelo">Modelo del producto:</label><input type="text" name="modelo" id="form-modelo" placeholder="XHRL234" value="<?= !empty($_POST['modelo'])?$_POST['modelo']:$_GET['modelo'] ?>"></li>

            <li><label for="form-precio">Precio:</label> <input min="99.99" type="text" step="0.01" name="precio" id="form-precio" placeholder="1000.00" value="<?= !empty($_POST['precio'])?$_POST['precio']:$_GET['precio'] ?>"></li>

            <li><label for="form-unidades">Unidades:</label> <input type="text" name="unidades" id="form-unidades" placeholder="11" value="<?= !empty($_POST['unidades'])?$_POST['unidades']:$_GET['unidades'] ?>"></li>

            <li><label for="form-imagen" class="imagen">Imagen del producto / si no tienes una imagen se cargara una por defecto: </label><input type="file" name="imagen" id="formimagen"> </li>

            <li><label for="form-detalles">Ingresa los detalles del producto:</label><br><textarea name="detalles" rows="4" cols="60" id="form-detalles" placeholder="No más de 250 caracteres de longitud"></textarea></li>
        </ul>
    </fieldset>
    <div style="text-align: center;">
      <p>
        <input type="submit" value="Editar producto"> 
        <input type="reset">
    </p>
</div>

</form> 

</body>
</html>