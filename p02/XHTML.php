<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Variables en PHP</title>
</head>
<body style="background-color:#f5eef8;">
	<?php

		/* 

		$_myvar      //es una variable inicia con $ y contiene underscore
		$_7var       //es una variable ya que aunque tiene numero, esta variable inicia con $ y contiene underscore
		myvar        // no es una variable porque no inicia con $
		$myvar       // es una variable incia con $ y una letra
		$var7        //es una variable incia con $ y una letra
		$_element1   //es una variable ya que aunque tenga numero, incia con $ y una letra
		$house*5     //no lo es ya que contiene un signo aritmetico y no esta permitido aunque inicie con $

		*/

		?>

		<h1 style="color: #2e4053 ;">EJERCICIO 2</h1>
		<p style="color: red;"><i> Inciso a </i></p>

		<?php
		//ejercicio 2 iniciso a
		$a = "ManejadorSQL";
		$b = 'MySql';
		$c = &$a;
		?>

		<p>
			Soy a y contengo: <?php echo "$a" ?> 
		    <br/>
			Soy b y contengo: <?php echo "$b"?>
			<br/> 
			Soy c y contengo: <?php echo "$a"?> 
		</p>

		<p style="color: forestgreen;"><i> Incisos b y c </i></p>

		<?php
		$a = "PHP server";
		$b = &$a;
		?>

		<p>
			Soy el nuevo a y contengo: <?php echo "$a" ?> 
		    <br/>
			Soy el nuevo b y contengo: <?php echo "$b"?>
			<br/> 
			Soy otra vez c y contengo: <?php echo "$c"?> 
		</p>

		<?php unset($a, $b, $c); ?>

		<p style="color: chocolate;"><i> Incisos d </i></p>

		<p><b>Al volver a imprimir, los nuevos a y b apuntan al 
		contenido del nuevo a,esto sucede porque las primeras tres variables
		ya han sido impresas a la pantalla, por lo que el compilador vuelve a leer las nuevas
		asiganciones haciendo que ignore a las variables anteriores</b></p>

		<h1 style="color: #2980b9 ;">EJERCICIO 3</h1>

		<?php
		$a = "PHP5";
		echo "soy a: $a <br>";
		$z[] = &$a;
		echo('<pre>');
			print_r($z);
		echo('</pre>');
		$b = "5a version de PHP";
		settype($b,"integer"); //evitar etiqueta warning al imprimir c, al poner settype b solo queda con el valor de 5 ya que es un tipo integer
		echo "soy b: $b <br>";  
		$c = $b*10;
		echo "soy c: $c <br>";
		$a .= $b;
		echo "soy a: $a <br>";
		$b *= $c;
		echo "soy b: $b <br>";
		$z[0] = "MySQL";
		echo('<pre>');
			print_r($z);
		echo('</pre>');

		?>

		<h1 style="color: #148f77;">EJERCICIO 4</h1>

		<?php

		function test(){

			$a = "PHP5";
			echo 'Soy a, en el ambito gobal:' . $GLOBALS["a"] . "<br>";

			$z[] = &$a;
			echo('Soy el arreglo z apuntando hacia "a" globalmente <pre>');
				print_r($GLOBALS['z']);
			echo('</pre>');

			$b = "5a version de PHP";
			settype($b,"integer");
			echo 'Soy b, en el ambito gobal:' . $GLOBALS["b"] . "<br>";

			$c = $b*10;
			echo 'Soy c, en el ambito gobal:' . $GLOBALS["c"] . "<br>";

			$a .= $b;
			echo 'Soy a, en el ambito gobal:' . $GLOBALS["a"] . "<br>";

			$b *= $c;
			echo 'Soy b, en el ambito gobal:' . $GLOBALS["b"] . "<br>";

			$z[0] = "MySQL";
			echo('Soy el areflo z <pre>');
				print_r($GLOBALS['z']);
			echo('</pre>');

		}

		test();
		unset($a, $b, $c, $z);

		?>

		<h1 style="color: #512e5f;">EJERCICIO 5</h1>

		<?php
		//ejercicio 5
		$a = "7 personas";
		$b = (integer) $a;
		$a = "9E3";
		$c = (double) $a;
		?>

		<p>
			Soy a: <?php echo "$a" ?> 
		    <br/>
			Soy b: <?php echo "$b"?>
			<br/> 
			Soy c: <?php echo "$c"?> 
		</p>
		<?php unset($a, $b, $c);?>

		<h1 style="color: #d4ac0d;">EJERCICIO 6</h1>

		<?php
		$a = "0";
		$b = "TRUE";
		$c = FALSE;
		$d = ($a OR $b);
		$e = ($a AND $c);
		$f = ($a XOR $b);

		echo "soy a" . var_dump((bool) $a) . "<br>";
		echo "soy b" . var_dump((bool) $b) . "<br>";
		echo "soy c" . var_dump((bool) $c) . "<br>";
		echo "soy d" . var_dump((bool) $d) . "<br>";
		echo "soy e" . var_dump((bool) $e) . "<br>";
		echo "soy f" . var_dump((bool) $f) . "<br>";

		settype($c, "int");
		echo "Imprimiendo el booleano c $c <br>";
		settype($e, "int");
		echo "Imprimiendo el booleano e $e <br>";
		unset($a, $b, $c, $d, $e, $f);

		?>

		<h1 style="color: #9c640c;">EJERCICIO 7</h1>
		<p >
			<b>Version de Apache y PHP ......</b> <?php echo ($_SERVER['SERVER_SOFTWARE']) ?>
			<br/>
			<b>Nombre del Sistema Opertivo (servidor) ......</b> <?php echo ($_SERVER['SERVER_NAME']) ?>
			<br/>
			<b>Idioma del navegador .......</b> <?php echo ($_SERVER['HTTP_ACCEPT_LANGUAGE'])  ?>	
		</p>

		<p>
			<a href="http://validator.w3.org/check?uri=referer">
			<img src="http://www.w3.org/Icons/valid-xhtml11" alt="Valid XHTML 1.1" height="31" width="88" /></a>
		</p>

	
</body>
</html>