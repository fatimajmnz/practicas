<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<?php

			if(isset($_GET['tope']))
			{
				$tope = $_GET['tope'];
			}
			else
			{
				die('Parámetro "tope" no detectado...');
			}

			if (!empty($tope))
			{
				/** SE CREA EL OBJETO DE CONEXION */
				@$link = new mysqli('localhost', 'root', 'Tormenta2101@', 'marketzone');	

				/** comprobar la conexión */
				if ($link->connect_errno) 
				{
					die('Falló la conexión: '.$link->connect_error.'<br/>');
					/** NOTA: con @ se suprime el Warning para gestionar el error por medio de código */
				}
?>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Producto</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"/>
</head>
<body>
	<h3>LISTA DE PRODUCTOS</h3>
	<table class="table">
		<thead class="thead-dark">
			<tr>
				<th scope="col">#</th>
				<th scope="col">Nombre</th>
				<th scope="col">Marca</th>
				<th scope="col">Modelo</th>
				<th scope="col">Precio</th>
				<th scope="col">Unidades</th>
				<th scope="col">Detalles</th>
				<th scope="col">Imagen</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$result = $link->query("SELECT * FROM productos WHERE unidades <= $tope");
			//mostrara los productos con el numero de unidades<=tope

			/** Crear una tabla que no devuelve un conjunto de resultados */
			while( $mostrar = $result->fetch_array(MYSQLI_ASSOC) ) 
				{
					?>
					<tr>
						<th><?= $mostrar['id'] ?></th>
						<td><?= $mostrar['nombre'] ?></td>
						<td><?= $mostrar['marca'] ?></td>
						<td><?= $mostrar['modelo'] ?></td>
						<td><?= $mostrar['precio'] ?></td>
						<td scope="mostrar"><?= $mostrar['unidades'] ?></td>
						<td><?= utf8_encode($mostrar['detalles']) ?></td>
						<td><img src="<?= $mostrar['imagen'] ?>" alt="Imagen"/></td>
					</tr>

					<?php
				}
				$result->free();
				$link->close();
			} 

			if(empty($tope)) : ?>
				<script  type="text/javascript">
					alert('Mostrando 0 articulos');
				</script>

			<?php endif; ?>
		</tbody>
	</table>
</body>
</html>
