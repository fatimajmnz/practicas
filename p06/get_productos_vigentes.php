<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<?php

			if(isset($_GET['eliminado']))
			{
				$eliminado = $_GET['eliminado'];
			}
			else
			{
				die('Parámetro "eliminado" no detectado...');
			}

			if (!is_null($eliminado))
			{
				/** SE CREA EL OBJETO DE CONEXION */
				@$link = new mysqli('localhost', 'root', 'Tormenta2101@', 'marketzone');	

				/** comprobar la conexión */
				if ($link->connect_errno) 
				{
					die('Falló la conexión: '.$link->connect_error.'<br/>');
					/** NOTA: con @ se suprime el Warning para gestionar el error por medio de código */
				}
?>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Producto</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"/>
</head>
<body>
	<h3>LISTA DE PRODUCTOS DISPONIBLES</h3>
	<table class="table">
		<thead class="thead-dark">
			<tr>
				<th scope="col">#</th>
				<th scope="col">Nombre</th>
				<th scope="col">Marca</th>
				<th scope="col">Modelo</th>
				<th scope="col">Precio</th>
				<th scope="col">Unidades</th>
				<th scope="col">Detalles</th>
				<th scope="col">Imagen</th>
			</tr>
		</thead>
		<tbody>

			<?php

			    if ($eliminado != 0) {
			    	echo '<script>alert("Los articulos no existen")</script>';
			    }else{

			    	$result = $link->query("SELECT * FROM productos WHERE eliminado <> 1 ");
			//mostrara toda la tabla hasta el limite tope
			    	/** Crear una tabla que no devuelve un conjunto de resultados */
			    	while( $mostrar = $result->fetch_array(MYSQLI_ASSOC) )
			    	{
			    		?>
			    		<tr>
			    			<th><?php echo $mostrar['id']; ?></th>
			    			<td><?php echo $mostrar['nombre'];?></td>
			    			<td><?php echo $mostrar['marca']; ?></td>
			    			<td><?php echo $mostrar['modelo']; ?></td>
			    			<td><?php echo $mostrar['precio']; ?></td>
			    			<td><?php echo $mostrar['unidades']; ?></td>
			    			<td><?php echo utf8_encode($mostrar['detalles']); ?></td>
			    			<td><img src= "<?php echo $mostrar['imagen']; ?>" alt="Imagen"/></td>
			    		</tr>

			    		<?php
			    	}
			    }
			}
			if(is_null($eliminado)) : 
				echo '<script>alert("Ningun producto.")</script>';

		endif; ?>
		
		</tbody>
	</table>
</body>
</html>
